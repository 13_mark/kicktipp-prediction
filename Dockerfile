FROM rocker/r-ubuntu:20.04

WORKDIR /usr/kicktipp-prediction

RUN R -e "install.packages('readr', dependencies = TRUE, repos='http://cran.rstudio.com/')"

COPY ./ /usr/kicktipp-prediction

CMD Rscript predict-this-weeks-games.R